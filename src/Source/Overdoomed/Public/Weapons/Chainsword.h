// Ludum Dare 50

#pragma once

#include "Weapon.h"

#include "Chainsword.generated.h"

USTRUCT(BlueprintType)
struct FChainswordSlash
{
    GENERATED_BODY();

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class UCameraShakeBase> Camera;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<class AActor> Threshold;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    class USoundCue* Sound;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    class UNiagaraSystem* Particles;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    class UCurveFloat* Lerp;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FTransform Start;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FTransform End;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Time;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Damage;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Radius;

    FChainswordSlash() = default;
    FChainswordSlash(TSubclassOf<class UCameraShakeBase> Camera, TSubclassOf<class AActor> Threshold, class USoundCue* Sound, class UNiagaraSystem* Particles, class UCurveFloat* Lerp, FTransform const& Start, FTransform const& End, float Time, float Damage, float Radius)
        : Camera(Camera)
        , Threshold(Threshold)
        , Sound(Sound)
        , Particles(Particles)
        , Lerp(Lerp)
        , Start(Start)
        , End(End)
        , Time(Time)
        , Damage(Damage)
        , Radius(Radius)
    {
    }
};

UCLASS(Abstract)
class AChainsword : public AWeapon
{
    GENERATED_BODY()

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class USceneComponent* PrimaryHandle;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* PrimaryMesh;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UNiagaraComponent* PrimaryTrail;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class USceneComponent* SecondaryHandle;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* SecondaryMesh;

public:
    AChainsword();

protected:
    virtual void Tick(float DeltaSeconds) override;

private:
    FChainswordSlash Slash;
    float SlashTimeStart;
    uint8 bSlashHit : 1;
    int32 SlashCombo;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Attack|Slash")
    class UCurveFloat* SlashCameraSlide;

    // Percentage at end of slash during which you can perform the next combo slash
    UPROPERTY(EditDefaultsOnly, Category = "Attack|Slash")
    float SlashComboGrace;

    // How much instant vertical force a successful aerial hit provides
    UPROPERTY(EditDefaultsOnly, Category = "Attack|Slash")
    float SlashHitJump;

    UPROPERTY(EditDefaultsOnly, Category = "Attack|Slash")
    TArray<FChainswordSlash> Slashes;

public:
    UFUNCTION(BlueprintCallable, Category = "Attack|Slash")
    bool IsSlashing() const;

    UFUNCTION(BlueprintCallable, Category = "Attack|Slash")
    bool CanComboSlash() const;

    UFUNCTION(BlueprintCallable, Category = "Attack|Slash")
    float GetLinearSlashProgress() const;

    UFUNCTION(BlueprintCallable, Category = "Attack|Slash")
    float GetSlashProgress() const;

private:
    FHitResult ThrowHit;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Attack|Throw")
    class USoundCue* ThrowSound;

    UPROPERTY(EditDefaultsOnly, Category = "Attack|Throw")
    float ThrowSpeed;

    UPROPERTY(EditDefaultsOnly, Category = "Attack|Throw")
    class USoundCue* ThrowHitSound;

    UPROPERTY(EditDefaultsOnly, Category = "Attack|Throw")
    class USoundCue* PullSound;

    UPROPERTY(EditDefaultsOnly, Category = "Attack|Throw")
    class UNiagaraSystem* PullParticles;

    UPROPERTY(EditDefaultsOnly, Category = "Attack|Throw")
    float PullStrength;

public:
    UFUNCTION(BlueprintCallable, Category = "Attack|Throw")
    bool IsThrowing() const;

    UFUNCTION(BlueprintCallable, Category = "Attack|Throw")
    bool IsPulling() const;

    UFUNCTION(BlueprintCallable, Category = "Attack|Throw")
    void StopThrow();

protected:
    virtual bool CanAttack_Implementation() const override;
    virtual void PerformPrimaryAttack_Implementation() override;
    virtual void PerformSecondaryAttack_Implementation() override;
    virtual void TryStopAttack_Implementation() override;
};
