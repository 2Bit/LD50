// Ludum Dare 50

#pragma once

#include "CoreMinimal.h"

#include "Weapon.generated.h"

UCLASS(Abstract)
class AWeapon : public AActor
{
    GENERATED_BODY()

private:
    int32 UpgradeLevel;

public:
    FORCEINLINE int32 GetUpgradeLevel() const { return this->UpgradeLevel; }

    UFUNCTION(BlueprintCallable, Category = "Upgrade")
    void SetUpgradeLevel(int32 NewUpgradeLevel);

    UFUNCTION(BlueprintNativeEvent, Category = "Attack")
    bool CanAttack() const;

    UFUNCTION(BlueprintNativeEvent, Category = "Attack")
    void PerformPrimaryAttack();

    UFUNCTION(BlueprintNativeEvent, Category = "Attack")
    void PerformSecondaryAttack();

    UFUNCTION(BlueprintNativeEvent, Category = "Attack")
    void TryStopAttack();

protected:
    virtual bool CanAttack_Implementation() const { return true; }
    virtual void PerformPrimaryAttack_Implementation() {}
    virtual void PerformSecondaryAttack_Implementation() {}
    virtual void TryStopAttack_Implementation() {}
};
