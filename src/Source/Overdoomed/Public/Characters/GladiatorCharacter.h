// Ludum Dare 50

#pragma once

#include "GameFramework/Character.h"

#include "GladiatorCharacter.generated.h"

UCLASS(Abstract, config = Game)
class AGladiatorCharacter : public ACharacter
{
    GENERATED_BODY()

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* Camera;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    class USkeletalMeshComponent* ArmsMesh;

public:
    AGladiatorCharacter();

protected:
    virtual void BeginPlay() override;
    virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
    virtual void Tick(float DeltaSeconds) override;
    virtual void Jump() override;
    virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

public:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Input")
    float BaseTurnRate;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Input")
    float BaseLookUpRate;

protected:
    void MoveForward(float Value);
    void MoveRight(float Value);
    void TurnAtRate(float Value);
    void LookUpAtRate(float Value);

public:
    UFUNCTION(BlueprintCallable, Category = "Game")
    void Forfeit();

private:
    float FootstepTime;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Movement|Walk")
    class UCurveFloat* HeadBob;

    UPROPERTY(EditDefaultsOnly, Category = "Movement|Walk")
    class USoundCue* FootstepSound;

    // Time between my buttcheeks scaled by character maximum walk speed (at max speed, one footstep every interval)
    UPROPERTY(EditDefaultsOnly, Category = "Movement|Walk")
    float FootstepInterval;

public:
    UFUNCTION(BlueprintCallable, Category = "Movement|Walk")
    bool IsWalking() const;

private:
    float DefaultCameraFOV;

    float DashTime;
    FVector DashDirectionWorld;
    FVector DashDirectionLocal;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Movement|Dash")
    class UCurveFloat* DashStrengthCurve;

    UPROPERTY(EditDefaultsOnly, Category = "Movement|Dash")
    class UCurveFloat* DashCameraCurve;

    UPROPERTY(EditDefaultsOnly, Category = "Movement|Dash")
    float DashCameraBob;

    UPROPERTY(EditDefaultsOnly, Category = "Movement|Dash")
    float DashCameraFOV;

    UFUNCTION(BlueprintImplementableEvent, Category = "Movement|Dash")
    void OnDashStart();

public:
    UFUNCTION(BlueprintCallable, Category = "Movement|Dash")
    bool IsDashing() const;

    UFUNCTION(BlueprintCallable, Category = "Movement|Dash")
    float GetDashProgress() const;

    UFUNCTION(BlueprintCallable, Category = "Movement|Dash")
    void Dash();

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Combat")
    class USoundCue* HurtSound;

private:
    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Combat|Weapon", meta = (AllowPrivateAccess = "true"))
    class AWeapon* Weapon;

    uint8 bIsAttacking : 1;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Combat|Weapon")
    TSubclassOf<class AWeapon> InitialWeapon;

public:
    FORCEINLINE class AWeapon* GetWeapon() const { return this->Weapon; }

    UFUNCTION(BlueprintCallable, Category = "Combat|Weapon")
    void SetWeapon(TSubclassOf<class AWeapon> NewWeaponClass);

    UFUNCTION(BlueprintCallable, Category = "Combat|Weapon")
    void PerformPrimaryAttack();

    UFUNCTION(BlueprintCallable, Category = "Combat|Weapon")
    void StartPrimaryAttack();

    UFUNCTION(BlueprintCallable, Category = "Combat|Weapon")
    void StopPrimaryAttack();

    UFUNCTION(BlueprintCallable, Category = "Combat|Weapon")
    void PerformSecondaryAttack();

private:
    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Combat|Kills", meta = (AllowPrivateAccess = "true"))
    int32 KillCombo;

    float KillComboTimeLeft;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Combat|Kills")
    float KillComboTime;

public:
    FORCEINLINE float GetKillCombo() const { return this->KillCombo; }

    UFUNCTION(BlueprintCallable, Category = "Combat|Kills")
    float GetKillComboProgress() const;

    UFUNCTION(BlueprintCallable, Category = "Combat|Kills")
    void AddKill();
};
