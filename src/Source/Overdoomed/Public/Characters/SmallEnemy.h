// Ludum Dare 50

#pragma once

#include "Enemy.h"

#include "SmallEnemy.generated.h"

UCLASS(Abstract)
class ASmallEnemy : public AEnemy
{
    GENERATED_BODY()
};
