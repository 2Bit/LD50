// Ludum Dare 50

#pragma once

#include "Enemy.h"

#include "LargeEnemy.generated.h"

UCLASS(Abstract)
class ALargeEnemy : public AEnemy
{
    GENERATED_BODY()
};
