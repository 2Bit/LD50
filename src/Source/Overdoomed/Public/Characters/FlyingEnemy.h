// Ludum Dare 50

#pragma once

#include "Enemy.h"

#include "FlyingEnemy.generated.h"

UCLASS(Abstract)
class AFlyingEnemy : public AEnemy
{
    GENERATED_BODY()
};
