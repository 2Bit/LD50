// Ludum Dare 50

#pragma once

#include "AIController.h"
#include "GameFramework/Character.h"

#include "Enemy.generated.h"

UCLASS()
class AEnemyController : public AAIController
{
    GENERATED_BODY()

public:
    virtual void Tick(float DeltaSeconds) override;
};

UCLASS(Abstract)
class AEnemy : public ACharacter
{
    GENERATED_BODY()

public:
    AEnemy();

    virtual void BeginPlay() override;
    virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
    virtual void Tick(float DeltaSeconds) override;
    virtual void Destroyed() override;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Lifetime")
    class UNiagaraSystem* SpawnParticles;

private:
    float StunTimeLeft;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Stun")
    FName StunMaterialParameter;

    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Combat")
    class USoundCue* StunSound;

    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Stun")
    float DamageStunDuration;

    UFUNCTION(BlueprintNativeEvent, Category = "Enemy|Stun")
    void OnStunBegin();
    virtual void OnStunBegin_Implementation() {}

    UFUNCTION(BlueprintNativeEvent, Category = "Enemy|Stun")
    void OnStunEnd();
    virtual void OnStunEnd_Implementation() {}

public:
    UFUNCTION(BlueprintCallable, Category = "Enemy|Stun")
    bool IsStunned() const;

    UFUNCTION(BlueprintCallable, Category = "Enemy|Stun")
    void Stun(float NewStunTime, bool PlayEffects = true);

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Combat")
    float Health;

    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Combat")
    float DestroyedFavour;

    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Combat")
    class USoundCue* AttackSound;

    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Combat")
    class UAnimMontage* AttackMontage;

    UPROPERTY(EditDefaultsOnly, Category = "Enemy|Combat")
    float AttackDistance;

public:
    float GetAttackDistance() const { return this->AttackDistance; }

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy|Combat")
    bool CanAct() const;

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy|Combat")
    bool IsAttacking() const;

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy|Combat")
    void Attack();

protected:
    virtual bool CanAct_Implementation() const;
    virtual bool IsAttacking_Implementation() const;
    virtual void Attack_Implementation();
};
