// Ludum Dare 50

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "HeadsTransformsBPFL.generated.h"

/**
 * 
 */
UCLASS()
class OVERDOOMED_API UHeadsTransformsBPFL : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

    UFUNCTION(BlueprintCallable)
    static void LookAtPlayer(UStaticMeshComponent* HeadStaticMesh, const float& DeltaTime, const float& InterpSpeed);

    UFUNCTION(BlueprintCallable)
    static void RotateAroundOrigin(UStaticMeshComponent* HeadStaticMesh, const FVector& StartingVector, const float& CurrentTime, const float& SecondsPerRotation);
};
