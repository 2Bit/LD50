// Ludum Dare 50

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"

#include "ArenaGameState.generated.h"

UCLASS(Abstract, minimalapi)
class AArenaGameState : public AGameStateBase
{
    GENERATED_BODY()

public:
    AArenaGameState();

protected:
    virtual void BeginPlay() override;

private:
    static int32 KillScore;

    float BeginTime;

public:
    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Game|Score")
    static int32 GetKillScore();

    UFUNCTION(BlueprintCallable, Category = "Game|Score")
    static void ModifyKillScore(int32 DeltaKillScore);

    UFUNCTION(BlueprintCallable, Category = "Game|Score")
    float GetDifficulty() const;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game|Favour")
    float Favour;

    UPROPERTY(EditDefaultsOnly, Category = "Combat")
    class USoundCue* LoseSound;

public:
    FORCEINLINE float GetFavour() const { return this->Favour; }

    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBegin);
    DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLose);

    UPROPERTY(BlueprintAssignable, Category = "Game|Favour")
    FOnBegin OnBegin;

    UPROPERTY(BlueprintAssignable, Category = "Game|Favour")
    FOnLose OnLose;

    UFUNCTION(BlueprintCallable, Category = "Game|Favour")
    void ModifyFavour(float DeltaFavour);
};
