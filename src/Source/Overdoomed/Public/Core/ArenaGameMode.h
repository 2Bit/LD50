// Ludum Dare 50

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "ArenaGameMode.generated.h"

USTRUCT(BlueprintType)
struct FArenaWave
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FText Name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<TSubclassOf<AEnemy>> Enemies;

    FArenaWave() = default;
    FArenaWave(FText const& Name)
        : Name(Name)
        , Enemies()
    {
    }
};

UCLASS(Abstract, minimalapi)
class AArenaGameMode : public AGameModeBase
{
    GENERATED_BODY()

public:
    AArenaGameMode();

protected:
    virtual void BeginPlay() override;
    virtual void Tick(float DeltaSeconds) override;

private:
    uint8 bChallenged : 1;

    UFUNCTION()
    void Challenge();

protected:
    // Drain rate per second
    UPROPERTY(EditDefaultsOnly, Category = "Game|Favour")
    float BaseFavourDrain;

    // Additional drain rate for every minute of play
    UPROPERTY(EditDefaultsOnly, Category = "Game|Favour")
    float IncrementalFavourDrain;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Game|Waves")
    float BaseMinimumEnemies;

    UPROPERTY(EditDefaultsOnly, Category = "Game|Waves")
    float IncrementalMinimumEnemies;

    UPROPERTY(EditDefaultsOnly, Category = "Game|Favour")
    class USoundCue* TutorialSound;

    UPROPERTY(EditDefaultsOnly, Category = "Game|Waves")
    float TutorialWaveTime;

    UPROPERTY(EditDefaultsOnly, Category = "Game|Waves")
    FArenaWave TutorialWave;

    UPROPERTY(EditDefaultsOnly, Category = "Game|Waves")
    TArray<FArenaWave> Waves;

public:
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNewWave, FArenaWave const&, Wave);

    UPROPERTY(BlueprintAssignable, Category = "Game|Waves")
    FOnNewWave OnNewWave;

    UFUNCTION(BlueprintCallable, Category = "Game|Waves")
    void SpawnEnemy(TSubclassOf<AEnemy> NewEnemyClass);

    UFUNCTION(BlueprintCallable, Category = "Game|Waves")
    void SpawnWave(FArenaWave const& NewWave);
};
