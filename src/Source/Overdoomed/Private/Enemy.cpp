// Ludum Dare 50

#include "Enemy.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Sound/SoundCue.h"

#include "Characters/GladiatorCharacter.h"
#include "Core/ArenaGameState.h"

void AEnemyController::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (AEnemy* const Self = Cast<AEnemy>(this->GetPawn()))
    {
        Self->GetCharacterMovement()->bOrientRotationToMovement = true;

        if (Self->CanAct())
        {
            APawn* const Target = this->GetWorld()->GetGameState()->PlayerArray[0]->GetPawn();
            if (Target != nullptr)
            {
                this->MoveToActor(Target, Self->GetAttackDistance());
                if (this->GetMoveStatus() == EPathFollowingStatus::Type::Idle)
                {
                    FVector TargetDirection = Target->GetActorLocation() - Self->GetActorLocation();
                    if (FMath::Abs(TargetDirection.Z) <= Self->GetSimpleCollisionHalfHeight())
                    {
                        TargetDirection.Z = 0.0f;
                        TargetDirection.Normalize();

                        this->SetControlRotation(TargetDirection.Rotation());
                        Self->GetCharacterMovement()->bOrientRotationToMovement = false;

                        if (FVector::DotProduct(Self->GetActorForwardVector(), TargetDirection) > 0.95f)
                        {
                            Self->Attack();
                        }
                    }
                }
            }
        }
        else
        {
            this->StopMovement();
        }
    }
}

AEnemy::AEnemy()
{
    this->AIControllerClass = AEnemyController::StaticClass();
    this->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

    this->bUseControllerRotationYaw = false;
    this->GetCharacterMovement()->bUseControllerDesiredRotation = true;

    this->Health = 100.0f;
    this->DestroyedFavour = 0.2f;

    this->StunMaterialParameter = "Stun";
    this->DamageStunDuration = 0.2f;

    this->AttackDistance = 150.0f;
}

void AEnemy::BeginPlay()
{
    Super::BeginPlay();

    this->GetMesh()->SetRelativeScale3D(FVector::ZeroVector);

    UNiagaraFunctionLibrary::SpawnSystemAtLocation(
        this,
        this->SpawnParticles,
        this->GetMesh()->GetComponentLocation());

    this->Stun(1.0f, false);
}

float AEnemy::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
    this->Health -= DamageAmount;

    if (this->Health <= 0.0f)
    {
        if (EventInstigator != nullptr)
        {
            if (AGladiatorCharacter* const KillingGladiator = Cast<AGladiatorCharacter>(EventInstigator->GetPawn()))
            {
                KillingGladiator->AddKill();
            }
        }

        this->Destroy();
    }
    else
    {
        this->Stun(this->DamageStunDuration);
    }

    return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void AEnemy::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (this->IsStunned())
    {
        this->StunTimeLeft -= this->GetWorld()->GetDeltaSeconds();
        if (this->StunTimeLeft <= 0.0f)
        {
            this->StunTimeLeft = 0.0f;

            this->GetMesh()->SetScalarParameterValueOnMaterials(StunMaterialParameter, 0.0f);
            this->CustomTimeDilation = 1.0f;

            this->OnStunEnd();
        }
    }

    this->GetMesh()->SetRelativeScale3D(
        FMath::Lerp(
            this->GetMesh()->GetRelativeScale3D(),
            FVector::OneVector,
            DeltaSeconds * 10.0f));
}

void AEnemy::Destroyed()
{
    if (AArenaGameState* const GameState = this->GetWorld()->GetGameState<AArenaGameState>())
    {
        GameState->ModifyFavour(this->DestroyedFavour);
    }

    Super::Destroyed();
}

bool AEnemy::IsStunned() const
{
    return this->StunTimeLeft > 0.0f;
}

void AEnemy::Stun(float NewStunTime, bool PlayEffects)
{
    if (NewStunTime > 0.0f)
    {
        this->StunTimeLeft += NewStunTime;
        if (this->StunTimeLeft <= NewStunTime)
        {
            this->GetMesh()->SetScalarParameterValueOnMaterials(StunMaterialParameter, 1.0f);
            this->CustomTimeDilation = 0.0f;

            this->OnStunBegin();
        }

        if (PlayEffects)
        {
            UGameplayStatics::PlaySoundAtLocation(this, this->StunSound, this->GetMesh()->GetComponentLocation());
        }
    }
}

bool AEnemy::CanAct_Implementation() const
{
    return !this->IsAttacking();
}

bool AEnemy::IsAttacking_Implementation() const
{
    return this->AttackMontage != nullptr && this->GetCurrentMontage() == this->AttackMontage;
}

void AEnemy::Attack_Implementation()
{
    this->PlayAnimMontage(this->AttackMontage);

    UGameplayStatics::PlaySoundAtLocation(this, this->AttackSound, this->GetMesh()->GetComponentLocation());
}
