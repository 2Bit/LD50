// Ludum Dare 50

#include "Weapons/Chainsword.h"

#include "Components/LineBatchComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Sound/SoundCue.h"

#include "Enemy.h"

AChainsword::AChainsword()
{
    this->RootComponent = this->CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

    this->PrimaryHandle = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Primary Handle"));
    this->PrimaryHandle->SetupAttachment(this->RootComponent);

    this->PrimaryMesh = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Primary Mesh"));
    this->PrimaryMesh->SetupAttachment(this->PrimaryHandle);

    this->PrimaryTrail = this->CreateDefaultSubobject<UNiagaraComponent>(TEXT("Primary Trail"));
    this->PrimaryTrail->bAutoActivate = false;
    this->PrimaryTrail->SetupAttachment(this->PrimaryMesh);

    this->SecondaryHandle = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Secondary Handle"));
    this->SecondaryHandle->SetRelativeLocation(FVector(0.0f, -25.0f, 0.0f));
    this->SecondaryHandle->SetupAttachment(this->RootComponent);

    this->SecondaryMesh = this->CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Secondary Mesh"));
    this->SecondaryMesh->SetupAttachment(this->SecondaryHandle);

    this->bSlashHit = false;
    this->SlashCombo = 0;
    this->SlashComboGrace = 0.35f;
    this->SlashHitJump = 400.0f;
    this->Slashes.Emplace(
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        nullptr,
        FTransform(
            FRotator(10.0f, 0.0f, 15.0f),
            FVector(40.0f, 15.0f, 30.0f)),
        FTransform(
            FRotator(-100.0f, 0.0f, 15.0f),
            FVector(40.0f, 15.0f, -50.0f)),
        0.2f,
        40.0f,
        200.0f);

    this->ThrowSpeed = 6000.0f;
    this->PullStrength = 5.0f;

    this->PrimaryActorTick.bCanEverTick = true;
    this->PrimaryActorTick.bStartWithTickEnabled = true;
}

void AChainsword::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    float PrimaryLerpSpeed = 10.0f;
    FTransform PrimaryHandleTarget = FTransform(
        FRotator::ZeroRotator,
        FVector(0.0f, 25.0f, 0.0f)
            + FVector(
                0.0f,
                FMath::Sin(this->GetWorld()->GetRealTimeSeconds() * 3.7f) * 0.8f,
                FMath::Sin(this->GetWorld()->GetRealTimeSeconds() * 5.2f) * 0.35f));

    if (this->IsSlashing())
    {
        this->PrimaryTrail->Activate();

        PrimaryLerpSpeed = 35.0f;

        float const SlashProgress = this->GetSlashProgress();
        PrimaryHandleTarget.Blend(this->Slash.Start, this->Slash.End, SlashProgress);

        if (SlashProgress >= 0.2f)
        {
            this->PrimaryTrail->Activate();
        }
        else
        {
            this->PrimaryTrail->Deactivate();
        }

        if (!this->bSlashHit && SlashProgress > 0.2f)
        {
            this->bSlashHit = true;

            if (this->Slashes.Num() - this->SlashCombo <= this->GetUpgradeLevel() && this->Slash.Threshold != nullptr)
            {
                FVector const SlashDelta = this->GetActorRotation().RotateVector(this->Slash.End.GetLocation() - this->Slash.Start.GetLocation());

                FVector const ThresholdNormal = FVector::CrossProduct(this->GetActorForwardVector(), SlashDelta.GetSafeNormal());

                AActor* const NewThreshold = this->GetWorld()->SpawnActor<AActor>(this->Slash.Threshold, this->GetActorLocation(), UKismetMathLibrary::MakeRotFromXZ(this->GetActorForwardVector(), ThresholdNormal));
                NewThreshold->SetInstigator(this->GetInstigator());
            }
            else
            {
                TArray<FHitResult> SlashHits;
                if (this->GetWorld()->SweepMultiByChannel(
                        SlashHits,
                        this->GetActorLocation() + this->GetActorForwardVector() * this->Slash.Radius,
                        this->GetActorLocation() + this->GetActorForwardVector() * this->Slash.Radius,
                        this->GetActorQuat(),
                        ECollisionChannel::ECC_Pawn,
                        FCollisionShape::MakeBox(FVector(this->Slash.Radius, this->Slash.Radius, 100.0f))))
                {
                    int32 EnemyCount = 0;
                    for (FHitResult const& SlashHit : SlashHits)
                    {
                        if (SlashHit.Actor->GetClass()->IsChildOf(AEnemy::StaticClass()))
                        {
                            ++EnemyCount;

                            if (this->Slash.Particles != nullptr)
                            {
                                FVector const SlashDelta = this->Slash.End.GetLocation() - this->Slash.Start.GetLocation();
                                UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, this->Slash.Particles, SlashHit.Location, SlashDelta.Rotation());
                            }
                        }

                        if (SlashHit.Actor != this->GetRootComponent()->GetAttachmentRootActor())
                        {
                            SlashHit.Actor->TakeDamage(this->Slash.Damage, FDamageEvent(), this->GetInstigator()->GetController(), this);
                        }
                    }

                    if (EnemyCount > 0)
                    {
                        UCharacterMovementComponent* const MovementComponent = this->GetRootComponent()->GetAttachmentRootActor()->FindComponentByClass<UCharacterMovementComponent>();
                        if (MovementComponent != nullptr && !MovementComponent->IsMovingOnGround())
                        {
                            MovementComponent->Velocity = FVector::ZeroVector;
                            MovementComponent->Velocity.Z = this->SlashHitJump;
                        }
                    }
                }
            }

            if (this->Slash.Camera != nullptr)
            {
                if (APawn* const OwnerPawn = Cast<APawn>(this->GetRootComponent()->GetAttachmentRootActor()))
                {
                    if (APlayerController* const OwnerPlayer = Cast<APlayerController>(OwnerPawn->GetController()))
                    {
                        OwnerPlayer->ClientStartCameraShake(this->Slash.Camera);
                    }
                }
            }
        }

        if (this->SlashCameraSlide != nullptr)
        {
            if (APawn* const OwnerPawn = Cast<APawn>(this->GetRootComponent()->GetAttachmentRootActor()))
            {
                FVector const SlashDelta = this->Slash.End.GetLocation() - this->Slash.Start.GetLocation();
                float const CameraSlideProgress = this->SlashCameraSlide->GetFloatValue(this->GetLinearSlashProgress());

                OwnerPawn->AddControllerYawInput(SlashDelta.Y * DeltaSeconds * CameraSlideProgress);
                OwnerPawn->AddControllerPitchInput(-SlashDelta.Z * DeltaSeconds * CameraSlideProgress);
            }
        }
    }
    else
    {
        this->PrimaryTrail->Deactivate();

        if (this->IsThrowing())
        {
            this->SecondaryMesh->AddLocalOffset(FVector(this->ThrowSpeed * DeltaSeconds, 0.0f, 0.0f), true, &this->ThrowHit);

            UCharacterMovementComponent* const MovementComponent = this->GetRootComponent()->GetAttachmentRootActor()->FindComponentByClass<UCharacterMovementComponent>();
            if (MovementComponent != nullptr)
            {
                MovementComponent->Velocity *= 0.4f;
            }

            if (this->ThrowHit.bBlockingHit)
            {
                if (this->PullParticles != nullptr)
                {
                    FVector const ThrowDelta = this->SecondaryMesh->GetComponentLocation() - this->SecondaryHandle->GetComponentLocation();
                    UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, this->PullParticles, this->ThrowHit.Location, ThrowDelta.Rotation());
                }

                if (AEnemy* const ThrowEnemy = Cast<AEnemy>(this->ThrowHit.Actor))
                {
                    ThrowEnemy->Stun(0.3f);
                }

                UGameplayStatics::PlaySound2D(this, this->ThrowHitSound);
            }
            else if ((this->SecondaryMesh->GetComponentLocation() - this->SecondaryHandle->GetComponentLocation()).SizeSquared() > 3000.0f * 3000.0f)
            {
                this->StopThrow();
            }
        }
        else if (this->IsPulling())
        {
            UCharacterMovementComponent* const MovementComponent = this->GetRootComponent()->GetAttachmentRootActor()->FindComponentByClass<UCharacterMovementComponent>();
            if (MovementComponent != nullptr && (this->SecondaryMesh->GetComponentLocation() - this->SecondaryHandle->GetComponentLocation()).SizeSquared() > 200.0f * 200.0f)
            {
                if (MovementComponent->IsMovingOnGround())
                {
                    MovementComponent->DoJump(false);
                }

                MovementComponent->Velocity = FVector(0.0f, 0.0f, FMath::Max(0.0f, MovementComponent->Velocity.Z));

                FHitResult Hit;
                this->GetRootComponent()->GetAttachmentRootActor()->AddActorWorldOffset((this->SecondaryMesh->GetComponentLocation() - this->SecondaryHandle->GetComponentLocation()) * this->PullStrength * DeltaSeconds, true, &Hit);

                if (Hit.bBlockingHit && Hit.Distance < 5.0f)
                {
                    this->StopThrow();
                }
            }
            else
            {
                this->StopThrow();
            }
        }
    }

    this->PrimaryHandle->SetRelativeLocation(
        FMath::Lerp(
            this->PrimaryHandle->GetRelativeLocation(),
            PrimaryHandleTarget.GetLocation(),
            FMath::Min(1.0f, DeltaSeconds * PrimaryLerpSpeed)));

    this->PrimaryHandle->SetRelativeRotation(
        FMath::Lerp(
            this->PrimaryHandle->GetRelativeRotation(),
            PrimaryHandleTarget.GetRotation().Rotator(),
            FMath::Min(1.0f, DeltaSeconds * PrimaryLerpSpeed)));

    if (!this->IsThrowing() && !this->IsPulling())
    {
        this->SecondaryMesh->SetRelativeLocation(
            FMath::Lerp(
                this->SecondaryMesh->GetRelativeLocation(),
                FVector::ZeroVector,
                FMath::Min(1.0f, DeltaSeconds * 35.0f)));

        this->SecondaryMesh->SetRelativeRotation(
            FMath::Lerp(
                this->SecondaryMesh->GetRelativeRotation(),
                FRotator::ZeroRotator,
                FMath::Min(1.0f, DeltaSeconds * 35.0f)));
    }
    else
    {
        this->GetWorld()->LineBatcher->DrawLine(
            this->SecondaryHandle->GetComponentLocation() + FMath::VRand() - this->GetActorForwardVector() * 20.0f,
            this->SecondaryMesh->GetComponentLocation() + FMath::VRand() * 10.0f,
            FLinearColor::White * 0.02f,
            1,
            3.0f,
            0.0f);

        // Rope look
        /*FVector const SecondaryStart = this->SecondaryHandle->GetComponentLocation() + FMath::VRand() - this->GetActorForwardVector() * 20.0f;
        FVector const SecondaryUp = this->SecondaryHandle->GetUpVector();
        FVector const SecondaryDelta = this->SecondaryMesh->GetComponentLocation() - this->SecondaryHandle->GetComponentLocation();
        float const SecondaryDeltaSize = SecondaryDelta.Size();

        float const Step = 0.025f;
        for (float i = 0; i < 1.0f; i += Step)
        {
            this->GetWorld()->LineBatcher->DrawLine(
                SecondaryStart + SecondaryDelta * i + SecondaryUp * FMath::Sin(this->GetWorld()->GetTimeSeconds() * 50.0f + SecondaryDeltaSize * i / 50.0f) * 10.0f,
                SecondaryStart + SecondaryDelta * (i + Step) + SecondaryUp * FMath::Sin(this->GetWorld()->GetTimeSeconds() * 50.0f + SecondaryDeltaSize * (i + Step) / 50.0f) * 10.0f,
                FLinearColor::White * 0.02f,
                1,
                3.0f,
                0.0f);
        }*/
    }
}

bool AChainsword::IsSlashing() const
{
    return (this->GetWorld()->GetTimeSeconds() - this->SlashTimeStart) < this->Slash.Time;
}

bool AChainsword::CanComboSlash() const
{
    if (this->IsSlashing())
    {
        return this->GetLinearSlashProgress() >= 1.0f - this->SlashComboGrace;
    }

    return false;
}

float AChainsword::GetLinearSlashProgress() const
{
    if (this->Slash.Time > 0.0f)
    {
        return FMath::Min(1.0f, (this->GetWorld()->GetTimeSeconds() - this->SlashTimeStart) / this->Slash.Time);
    }

    return 1.0f;
}

float AChainsword::GetSlashProgress() const
{
    if (this->Slash.Time > 0.0f)
    {
        float const LinearProgress = this->GetLinearSlashProgress();

        if (this->Slash.Lerp != nullptr)
        {
            return this->Slash.Lerp->GetFloatValue(LinearProgress);
        }

        return LinearProgress;
    }

    return 1.0f;
}

bool AChainsword::IsThrowing() const
{
    return this->SecondaryMesh->IsUsingAbsoluteLocation() && !this->ThrowHit.bBlockingHit;
}

bool AChainsword::IsPulling() const
{
    return this->ThrowHit.bBlockingHit;
}

void AChainsword::StopThrow()
{
    if (this->IsThrowing() || this->IsPulling())
    {
        this->ThrowHit = FHitResult();

        FTransform const SecondaryMeshWorldTransform = this->SecondaryMesh->GetComponentTransform();

        this->SecondaryMesh->SetAbsolute(false, false);
        this->SecondaryMesh->SetWorldLocation(SecondaryMeshWorldTransform.GetLocation());
        this->SecondaryMesh->SetWorldRotation(SecondaryMeshWorldTransform.GetRotation().Rotator());

        UGameplayStatics::PlaySound2D(this, this->PullSound);
    }
}

bool AChainsword::CanAttack_Implementation() const
{
    return (!this->IsSlashing() || this->CanComboSlash()) && !this->IsThrowing() && !this->IsPulling();
}

void AChainsword::PerformPrimaryAttack_Implementation()
{
    if (this->CanAttack())
    {
        if (!this->CanComboSlash())
        {
            this->SlashCombo = 0;
        }
        else
        {
            ++this->SlashCombo;
            if (this->SlashCombo >= this->Slashes.Num())
            {
                this->SlashCombo = 0;
            }
        }

        this->Slash = this->Slashes[this->SlashCombo];
        this->SlashTimeStart = this->GetWorld()->GetTimeSeconds();
        this->bSlashHit = false;

        UGameplayStatics::PlaySound2D(this, this->Slash.Sound);

        this->PrimaryTrail->Activate();
    }
}

void AChainsword::PerformSecondaryAttack_Implementation()
{
    if (this->CanAttack())
    {
        if ((this->SecondaryMesh->GetComponentLocation() - this->SecondaryHandle->GetComponentLocation()).SizeSquared() < 50.0f * 50.0f)
        {
            this->SecondaryMesh->SetRelativeTransform(FTransform::Identity);

            FTransform const SecondaryMeshWorldTransform = this->SecondaryMesh->GetComponentTransform();

            this->SecondaryMesh->SetAbsolute(true, true);
            this->SecondaryMesh->SetWorldLocation(SecondaryMeshWorldTransform.GetLocation());
            this->SecondaryMesh->SetWorldRotation(SecondaryMeshWorldTransform.GetRotation().Rotator());

            UGameplayStatics::PlaySound2D(this, this->ThrowSound);
        }
    }
}

void AChainsword::TryStopAttack_Implementation()
{
    this->StopThrow();
}
