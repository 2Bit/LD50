// Ludum Dare 50


#include "HeadsTransformsBPFL.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

void UHeadsTransformsBPFL::LookAtPlayer(UStaticMeshComponent* HeadStaticMesh, const float& DeltaTime, const float& InterpSpeed)
{
    const FVector PlayerLocation = UGameplayStatics::GetPlayerPawn(HeadStaticMesh->GetWorld(), 0)->GetActorLocation();
    const FVector HeadLocation = HeadStaticMesh->GetComponentLocation();

    const FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(HeadLocation, PlayerLocation);
    const FRotator NewRotation = FMath::RInterpTo(HeadStaticMesh->GetComponentRotation(), LookAtRotation, DeltaTime, InterpSpeed);

    HeadStaticMesh->SetWorldRotation(NewRotation);	
}

void UHeadsTransformsBPFL::RotateAroundOrigin(UStaticMeshComponent* HeadStaticMesh, const FVector& StartingVector, const float& CurrentTime, const float& SecondsPerRotation)
{
    FVector TargetLocation = StartingVector.RotateAngleAxis(FMath::Fmod(CurrentTime, SecondsPerRotation) / SecondsPerRotation * 360, FVector::UpVector);

    if (HeadStaticMesh->GetAttachmentRoot())
    {
        TargetLocation += FVector(0, 0, HeadStaticMesh->GetAttachmentRoot()->GetComponentLocation().Z);
    }

    HeadStaticMesh->SetWorldLocation(TargetLocation);
}
