// Ludum Dare 50

#include "Core/ArenaGameMode.h"

#include "Components/CapsuleComponent.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"
#include "NavigationSystem.h"
#include "Sound/SoundCue.h"

#include "Characters/GladiatorCharacter.h"
#include "Core/ArenaGameState.h"
#include "Enemy.h"

AArenaGameMode::AArenaGameMode()
{
    this->DefaultPawnClass = AGladiatorCharacter::StaticClass();
    this->GameStateClass = AArenaGameState::StaticClass();

    this->BaseFavourDrain = 0.04f;
    this->IncrementalFavourDrain = 0.02f;

    this->BaseMinimumEnemies = 3;
    this->IncrementalMinimumEnemies = 9;

    this->TutorialWaveTime = 8.0f;

    this->PrimaryActorTick.bCanEverTick = true;
    this->PrimaryActorTick.bStartWithTickEnabled = true;
}

void AArenaGameMode::BeginPlay()
{
    Super::BeginPlay();

    if (AArenaGameState* const State = this->GetGameState<AArenaGameState>())
    {
        State->OnBegin.AddDynamic(this, &AArenaGameMode::Challenge);
    }
}

void AArenaGameMode::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (this->GetWorld()->GetTimeSeconds() >= this->TutorialWaveTime && this->GetWorld()->GetTimeSeconds() - this->TutorialWaveTime < DeltaSeconds)
    {
        this->SpawnWave(this->TutorialWave);

        UGameplayStatics::PlaySound2D(this, this->TutorialSound);
    }

    if (this->bChallenged)
    {
        if (AArenaGameState* const State = this->GetGameState<AArenaGameState>())
        {
            State->ModifyFavour(DeltaSeconds * -(this->BaseFavourDrain + State->GetDifficulty() * this->IncrementalFavourDrain));

            if (this->Waves.Num() > 0)
            {
                int32 const MinimumEnemies = this->BaseMinimumEnemies + State->GetDifficulty() * this->IncrementalMinimumEnemies;

                int32 EnemyCount = 0;
                for (TActorIterator<AEnemy> It(this->GetWorld()); It; ++It)
                {
                    ++EnemyCount;
                }

                if (EnemyCount < MinimumEnemies)
                {
                    this->SpawnWave(this->Waves[FMath::RandRange(0, this->Waves.Num() - 1)]);
                }
            }
        }
    }
}

void AArenaGameMode::Challenge()
{
    this->bChallenged = true;
}

void AArenaGameMode::SpawnEnemy(TSubclassOf<AEnemy> NewEnemyClass)
{
    if (NewEnemyClass != nullptr)
    {
        if (UNavigationSystemV1* const Navigation = UNavigationSystemV1::GetCurrent(this->GetWorld()))
        {
            FNavLocation Point;
            Navigation->GetRandomReachablePointInRadius(FVector::ZeroVector, 10000.0f, Point);

            FActorSpawnParameters SpawnParams;
            SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

            AEnemy* const NewEnemy = this->GetWorld()->SpawnActor<AEnemy>(
                NewEnemyClass,
                Point.Location + FVector(0.0f, 0.0f, NewEnemyClass.GetDefaultObject()->GetCapsuleComponent()->GetScaledCapsuleHalfHeight()),
                FRotator(0.0f, 0.0f, FMath::Rand() * 360.0f),
                SpawnParams);
        }
    }
}

void AArenaGameMode::SpawnWave(FArenaWave const& NewWave)
{
    for (int i = 0; i < NewWave.Enemies.Num(); ++i)
    {
        this->SpawnEnemy(NewWave.Enemies[i]);
    }

    this->OnNewWave.Broadcast(NewWave);
}
