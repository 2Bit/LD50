// Ludum Dare 50

#include "Core/ArenaGameState.h"

#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

int32 AArenaGameState::KillScore = -1;

AArenaGameState::AArenaGameState()
{
    this->Favour = 0.5f;
}

void AArenaGameState::BeginPlay()
{
    Super::BeginPlay();

    AArenaGameState::KillScore = 0;
}

int32 AArenaGameState::GetKillScore()
{
    return AArenaGameState::KillScore;
}

void AArenaGameState::ModifyKillScore(int32 DeltaKillScore)
{
    AArenaGameState::KillScore += DeltaKillScore;
}

float AArenaGameState::GetDifficulty() const
{
    return this->BeginTime <= 0.0f ? 0.5f : (this->GetWorld()->GetTimeSeconds() - this->BeginTime) / 60.0f + 0.5f;
}

void AArenaGameState::ModifyFavour(float DeltaFavour)
{
    if (this->Favour > 0.0f)
    {
        if (DeltaFavour > 0.0f)
        {
            if (AArenaGameState::KillScore <= 0)
            {
                this->BeginTime = this->GetWorld()->GetTimeSeconds();

                this->OnBegin.Broadcast();
            }

            AArenaGameState::ModifyKillScore(DeltaFavour * 400.0f * this->GetDifficulty());
        }

        this->Favour = FMath::Min(1.0f, this->Favour + DeltaFavour);
        if (this->Favour <= 0.0f)
        {
            this->Favour = 0.0f;

            this->OnLose.Broadcast();

            UGameplayStatics::PlaySound2D(this, this->LoseSound);
        }
    }
}
