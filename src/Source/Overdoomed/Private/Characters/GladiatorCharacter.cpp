// Ludum Dare 50

#include "Characters/GladiatorCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Curves/CurveFloat.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

#include "Core/ArenaGameState.h"
#include "Weapon.h"

AGladiatorCharacter::AGladiatorCharacter()
{
    this->GetCapsuleComponent()->InitCapsuleSize(55.0f, 96.0f);

    USceneComponent* const Head = this->CreateDefaultSubobject<USceneComponent>(TEXT("Head"));
    Head->SetRelativeLocation(FVector(-40.0f, 0.0f, 64.0f));
    Head->SetupAttachment(this->GetCapsuleComponent());

    this->Camera = this->CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    this->Camera->bUsePawnControlRotation = true;
    this->Camera->SetupAttachment(Head);

    this->ArmsMesh = this->CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Arms Mesh"));
    this->ArmsMesh->SetupAttachment(this->Camera);

    this->JumpMaxCount = 2;

    this->BaseTurnRate = 45.0f;
    this->BaseLookUpRate = 45.0f;

    this->FootstepInterval = 0.35f;

    this->DashCameraBob = 10.0f;
    this->DashCameraFOV = 110.0f;
    this->DashTime = 10000.0f;

    this->KillComboTime = 5.0f;
}

void AGladiatorCharacter::BeginPlay()
{
    Super::BeginPlay();

    if (this->DefaultCameraFOV <= 0.0f)
    {
        this->DefaultCameraFOV = this->Camera->FieldOfView;
    }

    this->SetWeapon(this->InitialWeapon);

    if (AArenaGameState* const GameState = this->GetWorld()->GetGameState<AArenaGameState>())
    {
        GameState->OnLose.AddDynamic(this, &AGladiatorCharacter::Forfeit);
    }
}

float AGladiatorCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
    if (AArenaGameState* const GameState = this->GetWorld()->GetGameState<AArenaGameState>())
    {
        GameState->ModifyFavour(-DamageAmount / 100.0f);
    }

    if (!this->GetController()->IsMoveInputIgnored())
    {
        UGameplayStatics::PlaySound2D(this, this->HurtSound);
    }

    return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void AGladiatorCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (this->IsDashing())
    {
        float const DashStrength = this->DashStrengthCurve->GetFloatValue(this->DashTime);

        this->AddActorWorldOffset(this->DashDirectionWorld * DashStrength * DeltaSeconds, true);

        if (this->DashCameraCurve != nullptr)
        {
            float const DashProgress = this->GetDashProgress();
            float const DashCameraWeight = this->DashCameraCurve->GetFloatValue(DashProgress);

            this->Camera->SetRelativeLocation(FVector(0.0f, 0.0f, -this->DashCameraBob * DashCameraWeight));
            this->Camera->SetFieldOfView(FMath::Lerp(this->DefaultCameraFOV, this->DashCameraFOV, DashCameraWeight));
        }

        this->DashTime += DeltaSeconds;
    }
    else
    {
        // Only play footstep sounds in we are actually "walking", e.g. we don't want footsteps when we jump.
        if (this->IsWalking() && this->GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Walking)
        {
            this->FootstepTime += DeltaSeconds * (this->GetVelocity().Size() / this->GetCharacterMovement()->MaxWalkSpeed);
            if (this->FootstepTime >= this->FootstepInterval)
            {
                this->FootstepTime -= this->FootstepInterval;

                UGameplayStatics::PlaySoundAtLocation(
                    this,
                    this->FootstepSound,
                    this->GetActorLocation());
            }
        }
        else
        {
            this->FootstepTime = 0.0f;
        }

        float HeadHeight = 0.0f;
        if (this->HeadBob != nullptr)
        {
            HeadHeight = this->HeadBob->GetFloatValue(this->FootstepTime / this->FootstepInterval);
        }

        this->Camera->SetRelativeLocation(FVector(0.0f, 0.0f, HeadHeight));
        this->Camera->SetFieldOfView(
            FMath::Lerp(
                this->Camera->FieldOfView,
                this->DefaultCameraFOV,
                FMath::Min(1.0f, DeltaSeconds * 10.0f)));
    }

    if (this->KillComboTimeLeft > 0.0f)
    {
        this->KillComboTimeLeft -= DeltaSeconds;
        if (this->KillComboTimeLeft <= 0.0f)
        {
            this->KillCombo = 0;
        }
    }

    if (this->Weapon != nullptr)
    {
        this->Weapon->SetUpgradeLevel(this->KillCombo / 15);
    }

    if (this->bIsAttacking)
    {
        this->PerformPrimaryAttack();
    }
}

void AGladiatorCharacter::Jump()
{
    if (this->Weapon != nullptr)
    {
        this->Weapon->TryStopAttack();
    }

    Super::Jump();
}

void AGladiatorCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    check(PlayerInputComponent);

    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
    PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

    PlayerInputComponent->BindAction("Dash", IE_Pressed, this, &AGladiatorCharacter::Dash);

    PlayerInputComponent->BindAction("PrimaryAttack", IE_Pressed, this, &AGladiatorCharacter::StartPrimaryAttack);
    PlayerInputComponent->BindAction("PrimaryAttack", IE_Released, this, &AGladiatorCharacter::StopPrimaryAttack);
    PlayerInputComponent->BindAction("SecondaryAttack", IE_Pressed, this, &AGladiatorCharacter::PerformSecondaryAttack);

    PlayerInputComponent->BindAxis("MoveForward", this, &AGladiatorCharacter::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &AGladiatorCharacter::MoveRight);

    PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
    PlayerInputComponent->BindAxis("TurnRate", this, &AGladiatorCharacter::TurnAtRate);
    PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("LookUpRate", this, &AGladiatorCharacter::LookUpAtRate);
}

void AGladiatorCharacter::MoveForward(float Value)
{
    if (Value != 0.0f)
    {
        this->AddMovementInput(this->GetActorForwardVector(), Value);
    }
}

void AGladiatorCharacter::MoveRight(float Value)
{
    if (Value != 0.0f)
    {
        this->AddMovementInput(this->GetActorRightVector(), Value);
    }
}

void AGladiatorCharacter::TurnAtRate(float Value)
{
    this->AddControllerYawInput(Value * this->BaseTurnRate * this->GetWorld()->GetDeltaSeconds());
}

void AGladiatorCharacter::LookUpAtRate(float Value)
{
    this->AddControllerPitchInput(Value * this->BaseLookUpRate * this->GetWorld()->GetDeltaSeconds());
}

void AGladiatorCharacter::Forfeit()
{
    this->CustomTimeDilation = 0.05f;

    if (this->GetController() != nullptr)
    {
        this->GetController()->SetIgnoreMoveInput(true);
        this->GetController()->SetIgnoreLookInput(true);
    }

    if (this->Weapon != nullptr)
    {
        this->Weapon->CustomTimeDilation = 0.05f;
    }
}

bool AGladiatorCharacter::IsWalking() const
{
    return this->GetVelocity().SizeSquared2D() > 5.0f;
}

bool AGladiatorCharacter::IsDashing() const
{
    if (this->DashStrengthCurve != nullptr)
    {
        float MinTime, MaxTime;
        this->DashStrengthCurve->GetTimeRange(MinTime, MaxTime);

        return this->DashTime < MaxTime;
    }

    return false;
}

float AGladiatorCharacter::GetDashProgress() const
{
    if (this->DashStrengthCurve != nullptr)
    {
        float MinTime, MaxTime;
        this->DashStrengthCurve->GetTimeRange(MinTime, MaxTime);

        return (this->DashTime - MinTime) / (MaxTime - MinTime);
    }

    return 0.0f;
}

void AGladiatorCharacter::Dash()
{
    this->DashTime = 0.0f;

    if (this->IsWalking())
    {
        this->DashDirectionWorld = this->GetLastMovementInputVector();
        this->DashDirectionLocal = this->GetActorRotation().UnrotateVector(this->GetLastMovementInputVector());
    }
    else
    {
        this->DashDirectionWorld = -this->GetActorForwardVector();
        this->DashDirectionLocal = FVector(-1.0f, 0.0f, 0.0f);
    }

    if (this->Weapon != nullptr)
    {
        this->Weapon->TryStopAttack();
    }

    this->GetMovementComponent()->Velocity.Z = 300.0f;

    this->OnDashStart();
}

void AGladiatorCharacter::SetWeapon(TSubclassOf<AWeapon> NewWeaponClass)
{
    if (this->Weapon != nullptr)
    {
        if (this->Weapon->GetClass() == NewWeaponClass)
        {
            return;
        }

        this->Weapon->Destroy();

        this->Weapon = nullptr;
    }

    if (NewWeaponClass != nullptr)
    {
        FActorSpawnParameters SpawnParams;
        SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

        this->Weapon = this->GetWorld()->SpawnActor<AWeapon>(NewWeaponClass, SpawnParams);
        this->Weapon->AttachToComponent(this->ArmsMesh, FAttachmentTransformRules::SnapToTargetIncludingScale);
        this->Weapon->SetActorRelativeLocation(FVector(40.0f, 0.0f, -20.0f));
        this->Weapon->SetInstigator(this);

        TInlineComponentArray<UPrimitiveComponent*> const ThisComponents(this);
        for (UPrimitiveComponent* const ThisComponent : ThisComponents)
        {
            ThisComponent->IgnoreActorWhenMoving(this->Weapon, true);
        }

        TInlineComponentArray<UPrimitiveComponent*> const WeaponComponents(this->Weapon);
        for (UPrimitiveComponent* const WeaponComponent : WeaponComponents)
        {
            WeaponComponent->IgnoreActorWhenMoving(this, true);
        }
    }
}

void AGladiatorCharacter::PerformPrimaryAttack()
{
    if (this->Weapon != nullptr)
    {
        this->Weapon->PerformPrimaryAttack();
    }
}

void AGladiatorCharacter::StartPrimaryAttack()
{
    this->bIsAttacking = true;
}

void AGladiatorCharacter::StopPrimaryAttack()
{
    this->bIsAttacking = false;
}

void AGladiatorCharacter::PerformSecondaryAttack()
{
    if (this->Weapon != nullptr)
    {
        this->Weapon->PerformSecondaryAttack();
    }
}

float AGladiatorCharacter::GetKillComboProgress() const
{
    return 1.0f - this->KillComboTimeLeft / this->KillComboTime;
}

void AGladiatorCharacter::AddKill()
{
    ++this->KillCombo;
    this->KillComboTimeLeft = this->KillComboTime;
}
